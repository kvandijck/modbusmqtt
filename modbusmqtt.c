#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <unistd.h>
#include <endian.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <getopt.h>
#include <netdb.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <arpa/inet.h>
#include <mosquitto.h>

#include "lib/libt.h"
#include "lib/libe.h"
#include "lib/liburi.h"
#include "modbusmqtt.h"

#define NAME "modbusmqtt"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* constants */
#define MAX_GAP_REGS	8
#define MAX_REQ_REGS	127
#define MB_PROTOCOL	0
#define MB_READ		0x03
#define MB_WRITE_MULTI	0x10
#define MB_EI		0x2B
#define MB_EI_DEVID	0x0e

/* generic error logging */
#define LOG_MQTT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_MQTT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_MQTT conflict
#endif
static void mqttlog(int loglevel, const char *str);

/* MQTT v5 */
#ifndef MQTT_SUB_OPT_NO_LOCAL
#define MQTT_SUB_OPT_NO_LOCAL 0x04
#define MQTT_SUB_OPT_RETAIN_AS_PUBLISHED 0x08
#endif

/* safeguard our LOG_EXIT extension */
static int max_loglevel = LOG_WARNING;
static int logtostderr = -1;

__attribute__((unused))
static void set_loglevel(int new_loglevel)
{
	max_loglevel = new_loglevel;
	if (!logtostderr)
		setlogmask(LOG_UPTO(max_loglevel));
}

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel & LOG_PRIMASK, "%s", str);

	} else if ((loglevel & LOG_PRIMASK) <= max_loglevel) {
		struct timespec tv;
		char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}
	if (loglevel & LOG_MQTT) {
		mqttlog(loglevel, str);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(num)	strerror(num)

#define xfree(ptr)	{ if (ptr) free(ptr); (ptr) = NULL; }

/* program options */
static const char help_msg[] =
	NAME ": control/monitor modbus/tcp slave\n"
	"usage:	" NAME " [OPTIONS ...] -h HOSTNAME_OR_IP\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -h, --host=HOST	Specify modbus/tcp host\n"
	" -8, --port=PORT	port (default 502)\n"
	" -b, --modbus-address=ADDR	modbus address (0..255, default 1)\n"
	" -4, --ipv4		use ipv4 only\n"
	" -6, --ipv6		use ipv6 only\n"
	" -m, --mqtt=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -n, --dryrun		No action\n"
	" -i, --interval=TIME	Set request interval (default 5s)\n"
	" -w, --work=TOPIC	TOPIC base for working state (default 'modbus')\n"
	" -p, --profile=PROFILE	select profile, '?' for list\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "host", required_argument, NULL, 'h', },
	{ "port", required_argument, NULL, '8', },
	{ "modbus-address", required_argument, NULL, 'b', },
	{ "ipv4", no_argument, NULL, '4', },
	{ "ipv6", no_argument, NULL, '6', },
	{ "mqtt", required_argument, NULL, 'm', },
	{ "dryrun", required_argument, NULL, 'n', },
	{ "interval", required_argument, NULL, 'i', },
	{ "work", required_argument, NULL, 'w', },
	{ "profile", required_argument, NULL, 'p', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?h:8:b:46m:ni:w:p:";

/* config */
static int dryrun;
static int default_interval = 60;
static int default_schedule_interval = 5;
static int family = AF_UNSPEC;

static const char *remote_host;
static int remote_port = 502;
static int mb_address = 1;

static struct sockaddr_storage sockaddr;
static int sockaddrlen;
static const char *remote_ipstr;

static const char *work_topic = "modbus";
static int work_topiclen;
static const char *profilename;

static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 0;
static int mqtt_qos = 0;
static int pub_sent_mid, pub_complete_mid;

/* state */
static struct mosquitto *mosq;
static struct {
	union {
		struct {
			char *vendor, *product, *revision;
		};
		char *list[3];
	};
} devid;

/* state */
static int mbsock = -1;
static uint16_t tid;
/* remember if we received the complete set at least once */
static int initialized;
/* keep times */
static double t_req_0;

/* request queue */
static struct req_block {
	uint16_t tid;
	uint16_t addr, cnt;
} zreq[3];
int zreq_pos;
#define NZREQ	(sizeof(zreq)/sizeof(zreq[0]))

/* signal handler */
static volatile int sigterm;

static void signalrecvd(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		}
	}
}

static uint16_t get16(const void *dat)
{
	uint16_t val;

	memcpy(&val, dat, sizeof(val));
	return be16toh(val);
}
static uint32_t get32(const void *dat)
{
	uint32_t val;

	memcpy(&val, dat, sizeof(val));
	return be32toh(val);
}
static void put16(uint16_t val, void *dat)
{
	val = htobe16(val);
	memcpy(dat, &val, sizeof(val));
}
static void put32(uint32_t val, void *dat)
{
	val = htobe32(val);
	memcpy(dat, &val, sizeof(val));
}

__attribute__((unused))
static void dump_bin(const char *msg, const void *vdat, int len)
{
	const uint8_t *dat = vdat;
	char line[1024];
	char *str = line;
	int j;

	for (j = 0; j < len; ++j) {
		if (j && !(j % 4))
			*str++ = ' ';
		str += sprintf(str, "%02x", dat[j]);
	}
	mylog(LOG_DEBUG, "%s '%s'", msg, line);
}

static const char *const modbus_errstr[256] = {
	[1] = "illegal function",
	[2] = "illegal data address",
	[3] = "illegal data value",
	[4] = "server failure",
	[5] = "ack",
	[6] = "server busy",
	[7] = "nack",
	[8] = "memory parity error",
	[10] = "gateway path not available",
	[11] = "gateway target no response",
};

const struct profile *prof;
static const struct profile **prof_table;
static int prof_table_cnt;
static int prof_table_size;

#define NREGS	(prof->nelements)
#define END_EL	(prof->elements+prof->nelements)

static struct value *values;

static void profile_selected(void);
void register_profile(const struct profile *prof)
{
	if (prof_table_cnt >= prof_table_size) {
		prof_table_size += 16;
		prof_table = realloc(prof_table, prof_table_size * sizeof(*prof_table));
	}
	prof_table[prof_table_cnt++] = prof;
}

static const struct profile *select_profile(const char *profile)
{
	int j;
	const struct profile *it;

	for (j = 0; j < prof_table_cnt; ++j) {
		it = prof_table[j];

		if (!profile) {
			if (!it->vendor && !it->product && !it->revision)
				continue;
			if (!fnmatch(it->vendor ?: "*", devid.vendor ?: "", 0)
				&& !fnmatch(it->product ?: "*", devid.product ?: "", 0)
				&& !fnmatch(it->revision ?: "*", devid.revision ?: "", 0)
			   ) {
				goto found;
			}
		} else if (!strcasecmp(prof_table[j]->name, profile)) {
found:
			prof = prof_table[j];
			values = realloc(values, prof->nelements*sizeof(values[0]));
			memset(values, 0, prof->nelements*sizeof(values[0]));
			return prof;
		}
	}
	return NULL;
}

static inline struct value *element_value(const struct element *el)
{
	return values + (el - prof->elements);
}

static const char *lookup_i(int i, const struct lookup *table)
{
	for (; table->a; ++table) {
		if (table->id == i)
			return table->a;
	}
	return NULL;
}

/* next element to be requested */
static const struct element *nextel;
static const struct element *nextel_fast;

/* modbus */
static time_t mb_recv_time;

static void schedule_request(void);
static void mb_request_next_blk(void);
static void schedule_next(void *dat)
{
	mylog(LOG_NOTICE, "%s()", __func__);
	mb_request_next_blk();
}

static void remember_request(const struct req_block *req)
{
	--zreq_pos;
	if (zreq_pos < 0)
		zreq_pos = NZREQ-1;
	zreq[zreq_pos] = *req;
}

static void mb_publish_element(const struct element *el, struct value *val)
{
	char buf[128];
	const char *str;
	int maxlen;
	int j;
	static char bmbuf[1024];
	char *bmstr, *sep;
	int clear_0;
	int tx_for_heartbeat;
	time_t now = time(NULL);

		if (!(val->flags & VFL_NEW))
			return;
		if (el->fn)
			el->fn(el, val);
		if (el->flags & LOCAL)
			/* no pub */
			return;

		buf[0] = 0;
		maxlen = sizeof(buf);
		str = buf;

		/* schedule heartbeat transmission when recently received,
		 * and not recently sent */
		tx_for_heartbeat =
			el->heartbeat
			&& (now - val->sent) >= el->heartbeat
			&& (now - val->recvd) < el->heartbeat
			;

		if (el->flags & EFL_NAN) {
			if (get32(val->raw) == el->nanval) {
				str = "nan";
				goto ready;
			}
		}
		switch (el->type) {
		case T_STR:
			str = val->a;
			maxlen = el->nreg*2+1;
			break;
		case T_UINT:
			sprintf(buf, el->fmt ?: "%u", val->u);
			break;
		case T_INT:
			sprintf(buf, el->fmt ?: "%i", val->u);
			break;
		case T_FLOAT:
			sprintf(buf, el->fmt ?: "%f", val->f);
			break;
		case T_ENUM:
			sprintf(buf, el->fmt ?: "#%u", val->u);
			str = lookup_i(val->u, el->table) ?: buf;
			break;
		case T_BITMASK:
		case T_BITMASK_ALARM:
			clear_0 = el->type == T_BITMASK_ALARM || el->flags & BITMASK_0_CLEAR;
			sep = (el->type == T_BITMASK_ALARM) ? "\n" : ",";
			bmstr = bmbuf;
			/* start with empty string */
			*bmstr = 0;
			for (j = 0; j < 16; ++j) {
				sprintf(buf, el->fmt ?: "#%u", j + el->bm_idx*16);
				str = lookup_i(j, el->table) ?: buf;
				if (val->u & (1 << j)) {
					bmstr += snprintf(bmstr, sizeof(bmbuf)-(bmstr-bmbuf), "%s%s",
							(bmstr > bmbuf) ? sep : "", str);
				}
				if (!tx_for_heartbeat && !((val->u ^ val->bm_prev) & (1 << j)))
					continue;
				mqtt_pub(topicfmt("%s/%s/%s", work_topic, el->topic, str),
						(val->u & (1 << j)) ? "1" : (clear_0 ? "" : "0"), 1);
				if (el->type == T_BITMASK_ALARM) {
					mylog(LOG_WARNING | LOG_MQTT, "%s %s%s", remote_host, (val->u & (1 << j)) ? "*" : "~", str);
				}
			}
			str = bmbuf;
			/* collect all bits for clearing at the end */
			if (clear_0)
				/* no need to collect forever, just keep current info */
				val->bm_ever = val->u;
			else
				val->bm_ever |= val->u;
			break;
		default:
			str = NULL;
			break;
		}
ready:
		val->flags &= ~VFL_NEW;
		//printf("%s: %s\n", el->topic, str ?: "");
		/* publish, and save cache */
		if (tx_for_heartbeat || strcmp(val->strval ?: "<>", str ?: "")) {
			mqtt_pub(topicfmt("%s/%s", work_topic, el->topic), str ?: "", 1);
			val->sent = now;
			if (!val->strval)
				val->strval = malloc(maxlen);
			strcpy(val->strval, str);
		}
}

static void mb_all_recvd(int normal)
{
	const struct element *el;
	struct value *val;
	char buf[128];
	int wanted, recvd;

	/* mark as done */
	wanted = recvd = 0;
	for (el = prof->elements, val = values; el < END_EL; ++el, ++val) {
		if (val->flags & VFL_WANT)
			++wanted;
		if (val->flags & VFL_RECVD)
			++recvd;
		/* clear flags */
		val->flags &= ~(VFL_RECVD | VFL_WANT);
	}

	if (prof->complete)
		prof->complete(recvd, wanted);
	if (recvd < wanted) {
		mylog(LOG_WARNING, "recvd %i/%i in %.2fsec", recvd, wanted, libt_now() - t_req_0);
	} else if (recvd) {
		mylog(LOG_INFO, "recvd all in %.2fsec", libt_now() - t_req_0);

		time_t now = time(NULL);
		static const char tfmt[] = "%a %d %b %Y %H:%M:%S";
		strftime(buf, sizeof(buf), tfmt, localtime(&now));
		mqtt_pub(topicfmt("%s/seen", work_topic), payloadfmt("%llu", (long long)now), 1);
		mqtt_pub(topicfmt("%s/seen/local", work_topic), buf, 1);
		if (!initialized) {
			/* we received the complete set */
			initialized = 1;
			mqtt_pub(topicfmt("%s/state", work_topic), "1", 1);
		}
	}
}

static void mb_request(int reg, int cnt)
{
	uint8_t dat[8+4];

	put16(++tid, dat+0);
	put16(MB_PROTOCOL, dat+2);
	put16(6, dat+4); /* len */
	dat[6] = mb_address;
	dat[7] = MB_READ;
	put16(reg, dat+8);
	put16(cnt, dat+10);
	mylog(LOG_INFO, "%s %i+%i: tid %i", __func__, reg, cnt, tid);
	if (send(mbsock, dat, sizeof(dat), 0) < 0)
		mylog(LOG_ERR, "send %li: %s", (long)sizeof(dat), ESTR(errno));
	dump_bin("send", dat, sizeof(dat));
	libt_add_timeout(2, schedule_next, NULL);
	remember_request(&((struct req_block){
				.tid = tid,
				.addr = reg,
				.cnt = cnt,
				}));
}

static int mb_request_next_blk_ptr(const struct element **pnextel, int fast)
{
	const struct element *nextel = *pnextel;
	const struct element *el0, *ele, *elp;
	const struct value *val;

	if (nextel >= END_EL || !nextel)
		nextel = prof->elements;
	/* loop over next elements,
	 * el0 will point to the first element to request,
	 * ele will point to the last element to request,
	 * elp points the the previously considered element
	 * nextel to the first element to consider in next run
	 * optimization:
	 * it's more efficient to join requests than to split in smaller
	 * parts, but we only fill gaps if they are backed by actual registers.
	 */
	for (el0 = ele = elp = NULL; nextel < END_EL; ++nextel) {
		val = element_value(nextel);
		if (elp && ((elp->addr + elp->nreg) < nextel->addr)) {
			/* this is a gap */
			break;
		}
		if (ele && ((nextel->addr - (ele->addr + ele->nreg)) > MAX_GAP_REGS))
			/* gap becomes too large */
			break;
		if (el0 && ((nextel->addr - ele->addr) > MAX_REQ_REGS))
			/* request becomes too large */
			break;
		if (ele)
			/* remember previous element when we already schedule a request */
			elp = nextel;
		if (!fast && !(val->flags & VFL_WANT))
			continue;
		if (!fast && (nextel->interval == 1))
			/* skip fast items for slow interval */
			continue;
		if (fast && (nextel->interval != 1))
			continue;
		if (!el0) {
			/* assign first element */
			el0 = ele = elp = nextel;

		} else {
			ele = nextel;
		}
	}

	*pnextel = nextel;
	if (el0)
		mb_request(el0->addr, ele->addr + ele->nreg - el0->addr);
	return el0 != NULL;
}

static int do_fast = 1;
static void mb_request_fast_cb(void *dat)
{
	do_fast = 1;
}

static void mb_request_next_blk(void)
{
	if (do_fast) {
		if (mb_request_next_blk_ptr(&nextel_fast, 1))
			/* fast is busy */
			return;
		/* end reached, reschedule */
		do_fast = 0;
		libt_add_timeout(libt_timetointerval2(prof->fast_interval ?: 5, 0.1), mb_request_fast_cb, NULL);
	}
	if (nextel >= END_EL) {
		mb_all_recvd(1);
		nextel = NULL;
		schedule_request();
		return;
	}
	if (!mb_request_next_blk_ptr(&nextel, 0))
		/* end reached, reschedule */
		schedule_request();
}


static void request(void *dat)
{
	const struct element *el;
	struct value *val;
	time_t now = time(NULL);
	int min_interval = prof->normal_interval ?: default_interval;

	t_req_0 = libt_now();

	for (el = prof->elements, val = values; el < END_EL; ++el, ++val) {
		if (el->flags & WO)
			/* never request */
			continue;
		if (!(val->flags & VFL_VALID)) {
			val->flags |= VFL_WANT;
		} else if (!(el->flags & ONCE)) {
			if ((now - val->recvd) >= (el->interval ?: prof->normal_interval ?: default_interval))
				val->flags |= VFL_WANT;
		}
		if (el->interval && el->interval < min_interval)
			min_interval = el->interval;
	}

	nextel = NULL;
	mb_request_next_blk();
}

static void schedule_request(void)
{
	libt_add_timeout(libt_timetointerval2(prof->fast_interval ?: default_schedule_interval, 0.1), request, NULL);
}

static void mb_recvd_element(const struct element *el, const void *vdat, int len)
{
	struct value *v = element_value(el);

	v->recvd = mb_recv_time;

	if (len < sizeof(v->raw)) {
		if (!(v->flags & VFL_VALID) || memcmp(v->raw, vdat, len))
			v->flags |= VFL_NEW;
		memcpy(v->raw, vdat, len);
	}
	v->flags |= VFL_RECVD | VFL_VALID;
	if (el->type == T_STR) {
		/* allocate a fixed length, strings will always have the same max length */
		if (!v->a) {
			v->a = malloc(len+1);
			v->flags |= VFL_NEW;

		} else if (memcmp(v->a, vdat, len)) {
			v->flags |= VFL_NEW;
		}
		memcpy(v->a, vdat, len);
		v->a[len] = 0;

	} else if (el->type == T_INT && len == 2) {
		v->i = (int16_t)get16(vdat);
	} else if (el->type == T_FLOAT && len == 2) {
		v->f = (int16_t)get16(vdat) * el->factor;
	} else if (el->type == T_FLOAT && len == 4) {
		v->f = (int32_t)get32(vdat) * el->factor;
	} else if (len == 2) {
		v->u = get16(vdat);
	} else if (len == 4) {
		v->u = get32(vdat);
	}

	mb_publish_element(el, v);
}

static void mb_recvd_read_holding_regs(const void *vdat, int len)
{
	const uint8_t *rdat = vdat;
	int ltid, nreg, req_end_addr;
	const struct element *el;
	struct req_block *req;
	int j;

	mb_recv_time = time(NULL);
	ltid = get16(rdat+0);

	/* find request, start with last sent request */
	for (j = 0, req = zreq+zreq_pos; j < NZREQ; ++j, ++req) {
		if (req >= zreq+NZREQ)
			req = zreq;
		if (req->tid == ltid)
			goto found;
	}
	mylog(LOG_WARNING, "tid mismatch <%i", ltid);
	return;
found:
	if (j) {
		/* needed to look in history */
		mylog(LOG_NOTICE, "tid %i (reg %u+%u) is %i in history",
				req->tid, req->addr, req->cnt, j);
	}
	nreg = (len-8-1)/2;
	if (nreg != req->cnt) {
		mylog(LOG_WARNING, "req.len mismatch >%i, <%i", req->cnt, nreg);
		/* ignore length mismatches */
		goto done;
	}
	/* re-compute end_addr to avoid memory problems */
	req_end_addr = req->addr + nreg;

	for (el = prof->elements; el < END_EL; ++el) {
		if (el->addr >= req->addr && (el->addr + el->nreg) <= req_end_addr) {
			/* this element is in this response */
			mb_recvd_element(el, rdat + 9 + (el->addr - req->addr)*2, el->nreg*2);

		} else if (el->addr >= req_end_addr) {
			/* all done */
			break;
		}
	}
done:
	/* clear request cache */
	memset(req, 0, sizeof(*req));
}

static void mb_request_devid(int code /* 1..4 */, int obj);
static void mb_request_devid_repeat(void *dat)
{
	long v = (intptr_t)dat;

	mb_request_devid(v >> 8, v & 0xff);
}
static void *devid_compose(int code, int obj)
{
	return (void *)(intptr_t)((code << 8) + obj);
}
static void mb_request_devid(int code /* 1..4 */, int obj)
{
	uint8_t dat[7+4];

	put16(++tid, dat+0);
	put16(MB_PROTOCOL, dat+2);
	put16(11-6, dat+4); /* len */
	dat[6] = mb_address;
	dat[7] = MB_EI;
	dat[8] = MB_EI_DEVID;
	dat[9] = code;
	dat[10] = obj;
	mylog(LOG_INFO, "%s %i,%i: tid %i", __func__, code, obj, tid);
	if (send(mbsock, dat, sizeof(dat), 0) < 0)
		mylog(LOG_ERR, "send %li: %s", (long)sizeof(dat), ESTR(errno));
	dump_bin("send", dat, sizeof(dat));
	libt_add_timeout(2, mb_request_devid_repeat, devid_compose(code, obj));
}

static void mb_recvd_devid(void *vdat, int len)
{
	uint8_t *dat = vdat;
	int pos;

	if (len < 7+6) {
		mylog(LOG_WARNING, "%s: len %i < 13", __func__, len);
		goto done;
	}
	if (dat[7+1] != 0x0e) {
		mylog(LOG_WARNING, "%s: MEI %02x != 0e", __func__, dat[7+1]);
		goto done;
	}
	libt_remove_timeout(mb_request_devid_repeat, devid_compose(dat[7+2], dat[7+7]));
	int objid, objlen;
	for (pos = 7+7; pos < len; ) {
		objid = dat[pos];
		objlen = dat[pos+1];
		if (objid < sizeof(devid.list)/sizeof(devid.list[0])) {
			devid.list[objid] = strndup((char *)dat+pos+2, objlen);
		}
		mylog(LOG_INFO, "devid %i +%i '%.*s'", objid, objlen, objlen, (char *)dat+pos+2);
		pos += 2 + objlen;
	}
	if (dat[7+4]) {
		mb_request_devid(1, dat[7+5]);
		return;
	}
done:
	/* advance to normal processing */
	mylog(LOG_NOTICE, "found %s, %s, %s", devid.vendor, devid.product, devid.revision);
	if (!prof) {
		select_profile(NULL);
		if (!prof)
			mylog(LOG_ERR, "no profile found %s, %s, %s", devid.vendor, devid.product, devid.revision);
		mylog(LOG_NOTICE, "selected profile '%s'", prof->name);
		profile_selected();
	}

	/* schedule request immediately, replacing any already scheduled item */
	libt_add_timeout(0, request, NULL);
}

static void mb_recvd(int fd, void *dat)
{
	static uint8_t rdat[16*1024];
	static int fill;
	int ret, len, cons;

	libt_remove_timeout(schedule_next, NULL);
	ret = recv(fd, rdat+fill, sizeof(rdat)-fill, MSG_DONTWAIT);
	if (ret < 0)
		mylog(LOG_ERR, "recv failed");
	if (ret == 0)
		mylog(LOG_ERR, "remote EOF");
	fill += ret;

	for (cons = 0; cons < fill;) {
		if (fill < cons+8)
			/* not even complete header */
			return;
		len = get16(rdat+cons+4);
		if (fill < cons+6+len)
			/* wait for complete packet */
			return;
		dump_bin("recv", rdat+cons, len+6);
		if (rdat[cons+7] & 0x80) {
			int cmd = rdat[cons+7] & 0x7f;
			int errnum = rdat[cons+8];
			if (cmd == MB_READ && errnum == 6) {
				/* server busy */
				libt_add_timeout(2, schedule_next, NULL);
				goto pkt_done;

			} else if (cmd == MB_EI) {
				/* we must succeed 0x2b in order to select a profile */
				mylog(LOG_ERR, "DEVID failed: %i, %s",
					errnum, modbus_errstr[errnum] ?: "?");
				exit(1);
			}
			/* exception */
			mylog(LOG_WARNING, "tid %02x, cmd 0x%02x: err %i, %s",
					get16(rdat+cons+0), cmd, errnum,
					modbus_errstr[errnum] ?: "?");
		} else switch (rdat[cons+7]) {
		case MB_READ: /* read holding registers */
			mb_recvd_read_holding_regs(rdat+cons, len+6);
			break;
		case MB_WRITE_MULTI: /* read holding registers */
			mylog(LOG_INFO, "write ok");
			//mb_recvd_write_multi(rdat+cons, len+6);
			break;
		case MB_EI: /* read holding registers */
			mb_recvd_devid(rdat+cons, len+6);
			break;
		}
		mb_request_next_blk();
		/* next*/
pkt_done:
		cons += len+6;
	}
	if (cons >= fill)
		fill = 0;
	else {
		memmove(rdat, rdat+cons, fill - cons);
		fill -= cons;
	}
}

static int strtobin(const char *str, const struct element *el, void *buf, int len)
{
	if (len < el->nreg*2)
		return -1;
	if (!str)
		str = "";
	len = el->nreg*2;

	if (el->type == T_STR) {
		strncpy(buf, str, len);

	} else if (el->type == T_INT && el->nreg == 1) {
		put16(htobe16(strtol(str, NULL, 0)), buf);

	} else if (el->type == T_INT && el->nreg == 2) {
		put32(htobe32(strtol(str, NULL, 0)), buf);

	} else if (el->type == T_UINT && el->nreg == 1) {
		put16(htobe16(strtoul(str, NULL, 0)), buf);

	} else if (el->type == T_UINT && el->nreg == 2) {
		put32(htobe32(strtoul(str, NULL, 0)), buf);

	} else if (el->type == T_FLOAT && el->nreg == 1) {
		put16(htobe16(strtod(str, NULL)/el->factor), buf);

	} else if (el->type == T_FLOAT && el->nreg == 2) {
		put32(htobe32(strtod(str, NULL)/el->factor), buf);

	} else {
		mylog(LOG_WARNING, "don't know how to convert %s", el->topic);
		return -1;
	}
	return len;
}

static void element_write(const struct element *el, const char *str)
{
	uint8_t dat[7 +6 +el->nreg*2 +1];
	int pktlen = sizeof(dat)-1;

	put16(++tid, dat+0);
	put16(MB_PROTOCOL, dat+2); /* protocol */
	put16(6+el->nreg*2, dat+4); /* len */
	dat[6] = mb_address;
	dat[7] = MB_WRITE_MULTI;
	put16(el->addr, dat+8);
	put16(el->nreg, dat+10);
	dat[12] = el->nreg*2;
	if (strtobin(str, el, dat+13, el->nreg*2) < 0)
		return;
	mylog(LOG_INFO, "%s '%s': tid %i", __func__, str, tid);
	dump_bin("send", dat, pktlen);
	if (send(mbsock, dat, pktlen, 0) < 0) {
		mylog(LOG_WARNING, "send %i: %s", pktlen, ESTR(errno));
		return;
	}
	/* ask to resend */
	element_value(el)->flags &= ~VFL_VALID;
}

/* mqtt iface */
static char *topicfmt_str;
__attribute__((format(printf,1,2)))
const char *topicfmt(const char *fmt, ...)
{
	va_list va;

	xfree(topicfmt_str);
	va_start(va, fmt);
	vasprintf(&topicfmt_str, fmt, va);
	va_end(va);
	return topicfmt_str;
}
static char *payloadfmt_str;
__attribute__((format(printf,1,2)))
const char *payloadfmt(const char *fmt, ...)
{
	va_list va;

	xfree(payloadfmt_str);
	va_start(va, fmt);
	vasprintf(&payloadfmt_str, fmt, va);
	va_end(va);
	return payloadfmt_str;
}

void mqtt_pub(const char *topic, const char *payload, int retain)
{
	int ret;

	mylog(LOG_DEBUG, "mqtt:>%s %s", topic, payload ?: "<null>");
	ret = mosquitto_publish(mosq, &pub_sent_mid, topic, strlen(payload?:""), payload, mqtt_qos, retain);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
}

static void my_mqtt_pub(struct mosquitto *mosq, void *dat, int mid)
{
	pub_complete_mid = mid;
}

static inline int mqtt_pending(void)
{
	return pub_sent_mid != pub_complete_mid;
}

static void mqtt_sub(const char *topic, int opts)
{
	int ret;

	ret = mosquitto_subscribe_v5(mosq, NULL, topic, mqtt_qos, opts, NULL);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", topic, mosquitto_strerror(ret));
}

static void mqttlog(int loglevel, const char *msg)
{
	static const char *const prionames[] = {
		[LOG_EMERG] = "emerg",
		[LOG_ALERT] = "alert",
		[LOG_CRIT] = "crit",
		[LOG_ERR] = "err",
		[LOG_WARNING] = "warn",
		[LOG_NOTICE] = "notice",
		[LOG_INFO] = "info",
		[LOG_DEBUG] = "debug",
	};

	mqtt_pub(topicfmt("log/%s/%s", NAME, prionames[loglevel & LOG_PRIMASK]), msg, 0);
}

static void my_mqtt_log(struct mosquitto *mosq, void *userdata, int level, const char *str)
{
	static const int logpri_map[] = {
		MOSQ_LOG_ERR, LOG_ERR,
		MOSQ_LOG_WARNING, LOG_WARNING,
		MOSQ_LOG_NOTICE, LOG_NOTICE,
		MOSQ_LOG_INFO, LOG_INFO,
		MOSQ_LOG_DEBUG, LOG_DEBUG,
		0,
	};
	int j;

	for (j = 0; logpri_map[j]; j += 2) {
		if (level & logpri_map[j]) {
			//mylog(logpri_map[j+1], "[mosquitto] %s", str);
			return;
		}
	}
}

static int test_set_topic(const char *topic, const char *main, int retain)
{
	if (retain)
		return !strcmp(topic, main);
	int len = strlen(main);

	return strlen(topic) == len+4
		&& !strncmp(topic, main, len)
		&& !strcmp(topic+len, "/set");
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	char *topic, *payload;

	mylog(LOG_DEBUG, "mqtt:<%s %s", msg->topic, (char *)msg->payload ?: "<null>");

	if (!strncmp(work_topic, msg->topic, work_topiclen) && (msg->topic[work_topiclen] == '/')) {
		topic = msg->topic + work_topiclen+1;
		payload = (char *)msg->payload;
		const struct element *el;

		if (test_set_topic(topic, "cfg/loglevel", msg->retain)) {
			if (!msg->payloadlen)
				return;
			set_loglevel(strtoul(payload, NULL, 0));
			mqtt_pub(topicfmt("%s/%s", work_topic, "cfg/loglevel"), payloadfmt("%i", max_loglevel), 1);
		} else for (el = prof->elements; el < END_EL; ++el) {
			if (!(el->flags & RW))
				continue;
			/* don't accept retained set requests */
			if (test_set_topic(topic, el->topic, 0)) {
				mylog(LOG_NOTICE, "set for %s", el->topic);
				element_write(el, payload);
				return;
			}
		}
	}
}

static void mqtt_maintenance(void *dat)
{
	int ret;
	struct mosquitto *mosq = dat;

	ret = mosquitto_loop_misc(mosq);
	if (ret)
		mylog(LOG_ERR, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
	libt_add_timeout(2.3, mqtt_maintenance, dat);
}

static void recvd_mosq(int fd, void *dat)
{
	struct mosquitto *mosq = dat;
	int evs = libe_fd_evs(fd);
	int ret;

	if (evs & LIBE_RD) {
		/* mqtt read ... */
		ret = mosquitto_loop_read(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
	}
	if (evs & LIBE_WR) {
		/* flush mqtt write queue _after_ the timers have run */
		ret = mosquitto_loop_write(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
	}
}

void mosq_update_flags(void)
{
	if (mosq)
		libe_mod_fd(mosquitto_socket(mosq), LIBE_RD | (mosquitto_want_write(mosq) ? LIBE_WR : 0));
}

int dns_resolve(const char *hostname, int port)
{
	int ret;
	struct addrinfo *resolved, *h;
	void *ntopptr;

	/* resolve */
	struct addrinfo hints = {
		.ai_family = family,
		.ai_socktype = SOCK_STREAM,
	};
	char port_str[16];
	sprintf(port_str, "%u", port);
	ret = getaddrinfo(hostname, /*"mbap"*/port_str, &hints, &resolved);
	if (ret)
		mylog(LOG_ERR, "could not resolve host %s: %s", hostname, gai_strerror(ret));

	if (!resolved)
		mylog(LOG_ERR, "host %s didn't resolve", hostname);

	else {
		h = resolved;
		switch (h->ai_family) {
		case AF_INET6:
			ntopptr = &((struct sockaddr_in6*)h->ai_addr)->sin6_addr;
			break;
		case AF_INET:
		default:
			ntopptr = &((struct sockaddr_in*)h->ai_addr)->sin_addr;
			break;
		}
		static char ipstr[128];
		remote_ipstr = inet_ntop(h->ai_family, ntopptr, ipstr, sizeof(ipstr));
		sockaddrlen = h->ai_addrlen;
		memcpy(&sockaddr, h->ai_addr, sockaddrlen);
	}
	freeaddrinfo(resolved);
	return 0;
}

static void profile_selected(void)
{
	mqtt_sub(topicfmt("%s/#", work_topic), MQTT_SUB_OPT_NO_LOCAL);
}

int main(int argc, char *argv[])
{
	int opt, ret;
	int j;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'h':
		remote_host = optarg;
		break;
	case '8':
		remote_port = strtoul(optarg, NULL, 0);
		break;
	case 'b':
		mb_address = strtoul(optarg, NULL, 0) & 0xff;
		break;
	case '4':
		family = AF_INET;
		break;
	case '6':
		family = AF_INET6;
		break;
	case 'm':
		mqtt_host = optarg;
		break;
	case 'n':
		dryrun = 1;
		break;
	case 'i':
		default_interval = strtoul(optarg, NULL, 0) ?: 5;
		break;
	case 'w':
		work_topic = optarg;
		break;
	case 'p':
		if (!strcmp(optarg, "list") || !strcmp(optarg, "?")) {
			const struct profile *it;
			for (j = 0; j < prof_table_cnt; ++j) {
				it = prof_table[j];
				fprintf(stderr, "\t* %s", it->name);
				if (it->vendor || it->product || it->revision) {
					fprintf(stderr, "\t[VEND=%s,PROD=%s,REV=%s]"
						, it->vendor ?: "*"
						, it->product ?: "*"
						, it->revision ?: "*"
						);
				}
				fprintf(stderr, "\n");
			}
			exit(1);
		}

		profilename = optarg;
		break;
	}

	if (!remote_host)
		mylog(LOG_ERR, "no remote host defined");
	if (profilename) {
		if (!select_profile(profilename)) {
			mylog(LOG_ERR, "modbus profile '%s' unknown", profilename);
		}
		mylog(LOG_NOTICE, "selected profile '%s'", prof->name);
	}

	/* remote host setup */
	dns_resolve(remote_host, remote_port);

	mbsock = socket(sockaddr.ss_family, SOCK_STREAM, 0);
	if (mbsock < 0)
		mylog(LOG_ERR, "socket: %s", ESTR(errno));

	if (connect(mbsock, (void *)&sockaddr, sockaddrlen) < 0) {
		mylog(LOG_ERR, "connect: %s", ESTR(errno));
		exit(1);
	}
	libe_add_fd(mbsock, mb_recvd, NULL);

	/* prepare signalfd */
	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);
	/* for inside GDB */
	sigdelset(&sigmask, SIGINT);

	if (sigprocmask(SIG_BLOCK, &sigmask, NULL) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, signalrecvd, NULL);

	/* MQTT start */
	work_topiclen = strlen(work_topic);
	mosquitto_lib_init();
	mosq = mosquitto_new(topicfmt(NAME "-%i", getpid()), true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_log_callback_set(mosq, my_mqtt_log);
	mosquitto_message_callback_set(mosq, my_mqtt_msg);
	mosquitto_publish_callback_set(mosq, my_mqtt_pub);
	mosquitto_will_set(mosq, topicfmt("%s/state", work_topic), 4, "lost", mqtt_qos, 1);
	mosquitto_int_option(mosq, MOSQ_OPT_PROTOCOL_VERSION, 5);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive ?: 60);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* subscribe to topics */
	mqtt_sub(topicfmt("%s/cfg/#", work_topic), MQTT_SUB_OPT_NO_LOCAL);

	libt_add_timeout(0, mqtt_maintenance, mosq);
	libe_add_fd(mosquitto_socket(mosq), recvd_mosq, mosq);

	mqtt_pub(topicfmt("%s/state", work_topic), "start", 1);
	if (prof) {
		profile_selected();
		request(NULL);
	} else {
		mb_request_devid(1, 0);
	}
	/* core loop */
	for (; !sigterm;) {
		libt_flush();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}
	mylog(LOG_NOTICE, "terminating ...");
	if (!mqtt_qos)
		/* set qos >= 1, so that all is flushed */
		mqtt_qos = 1;

	const struct element *el;
	struct value *val;
	const char *str;
	char buf[32];
	for (el = prof->elements, val = values; el < END_EL; ++el, ++val) {
		if (val->strval)
			mqtt_pub(topicfmt("%s/%s", work_topic, el->topic), "", 1);
		if (el->type == T_BITMASK || el->type == T_BITMASK_ALARM) {
			for (j = 0; j < 16; ++j) {
				if (!(val->bm_ever & (1 << j)))
					continue;
				sprintf(buf, el->fmt ?: "#%u", j + el->bm_idx*16);
				str = lookup_i(j, el->table) ?: buf;
				mqtt_pub(topicfmt("%s/%s/%s", work_topic, el->topic, str), "", 1);
			}
		}
	}
	mqtt_pub(topicfmt("%s/state", work_topic), "stopped", 1);

	/* terminate */
	for (; mqtt_pending(); ) {
		libt_flush();
		mosq_update_flags();
		ret = libe_wait(libt_get_waittime());
		if (ret >= 0)
			libe_flush();
	}

#if 1
	xfree(payloadfmt_str);
	xfree(topicfmt_str);

	/* cleanup */
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return 0;
}
