# modbusmqtt

modbusmqtt is a bridge between a modbus-over-tcp slave and MQTT

# example use

Run these commands (or start with your init system).

	$ modbusmqtt -h HOST_OR_IP -w MQTT_PREFIX/ -v &

This will export the invertor properties into MQTT,
under MQTT_PREFIX/

controlling is possible by publishing to MQTT_PREFIX/PROP/set (non-retained).

# profiles

`modbusmqtt` supports different profiles.
Devices that support DEVID requests, can be autoprobed.
For other devices, supply `-p <PROFILE_NAME>` to the program

## huawei sun2000

## rut240

