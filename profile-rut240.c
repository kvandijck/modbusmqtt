#include "modbusmqtt.h"

static const struct element elements[] = {
	{ "uptime"	, 1, 2, T_UINT, },
	{ "rssi"	, 3, 2, T_FLOAT, .factor = 1, .fmt = "%.0f", },
	{ "temp"	, 5, 2, T_FLOAT, .factor = .1, .fmt = "%.1f", },
	{ "hostname"	, 7, 16, T_STR, RW, },
	{ "operator"	, 23, 16, T_STR, },
	{ "serial"	, 39, 16, T_STR, ONCE, },
	{ "mac"		, 55, 16, T_STR, ONCE, },
	{ "name"	, 71, 16, T_STR, ONCE | RW, },
	{ "network"	, 103, 16, T_STR, },
	{ "type"	, 119, 16, T_STR, },
	//{ "wanip"	, 139, 4, T_UINT, },
	{ "wifi/en"	, 203, 1, T_UINT, RW | WO, },
	{ "mobile/en"	, 204, 1, T_UINT, RW | WO, },
	{ "imsi"	, 348, 16, T_STR, },
};

static const struct profile profile = {
	.name = "RUT240",
	.elements = elements,
	.nelements = ARRAY_SIZE(elements),
};

__attribute__((constructor))
static void profile_init(void)
{
	register_profile(&profile);
}
