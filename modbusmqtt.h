#include <stdint.h>
#include <time.h>
#include <syslog.h>

#ifndef _solarmqtt_h_
#define _solarmqtt_h_

struct value;
struct element {
	const char *topic;
	int addr;
	int nreg;
	int type;
		#define T_FLOAT	0
		#define T_INT	1
		#define T_UINT	2
		#define T_STR	3
		#define T_ENUM	4
		#define T_BITMASK	5
		#define T_BITMASK_ALARM	6
	int flags;
		#define ONCE	(1 << 0)
		#define RW	(1 << 1)
		#define WO	(1 << 2)
		#define LOCAL	(1 << 3)
		#define EFL_NAN	(1 << 4)
		#define BITMASK_0_CLEAR	(1 << 5)
	unsigned int nanval;
	/* heartbeat
	 * interval after which payloads are published
	 * even when equal. This helps to detect timeouts
	 * on the receiving end */
	int heartbeat;
	/* interval: update period
	 * set to 1 to fetch As-Fast-as-Possible
	 */
	int interval;
	double factor;
	const char *fmt;
	/* enum's */
	const struct lookup {
		int id;
		const char *a;
	} *table;
	/* callback */
	struct {
		void (*fn)(const struct element *el, struct value *val);
		union {
			void *dat;
			int cookie;
		};
	};
	int bm_idx;
};

struct value {
	union {
		int i;
		unsigned int u;
		double f;
		char *a;
	};
	char *strval;
	/* raw value for numeric registers */
	uint16_t raw[4];
	/* previous bitmask value */
	uint16_t bm_prev;
	/* bitmask with all bits set that were ever set */
	uint16_t bm_ever;
	int flags;
		#define VFL_NEW		(1 << 0)
		#define VFL_RECVD	(1 << 1)
		#define VFL_VALID	(1 << 2)
		#define VFL_WANT	(1 << 3) /* we want this value, decided at start of request cycle */
	time_t recvd;
	time_t sent; /* when forwarded to mqtt */
};

#define ARRAY_SIZE(x)	(sizeof(x)/sizeof((x)[0]))

struct profile {
	const char *name;
	const char *vendor;
	const char *product;
	const char *revision;
	const struct element *elements;
	int nelements;
	void (*complete)(int done, int wanted);
	int fast_interval, normal_interval;
};

extern void register_profile(const struct profile *);
#define REGISTER_PROFILE(profile) \
static void init##profile(void) \
__attribute__((constructor)) \
{ register_profile(&profile); }

extern
__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...);
#define LOG_MQTT	0x4000000

extern
__attribute__((format(printf,1,2)))
const char *payloadfmt(const char *fmt, ...);

extern
__attribute__((format(printf,1,2)))
const char *topicfmt(const char *fmt, ...);

extern
void mqtt_pub(const char *topic, const char *payload, int retain);

#endif
